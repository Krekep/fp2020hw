open Minikanren_lib.Parser
open Minikanren_lib.Ast

exception Foo of string

let test_value =
  let temp = apply parser "run 12 (q) (== q b)" in
  match temp with
  | Some value -> print_string (program_to_str value)
  | None -> raise (Foo "EXCEPTION IN test_value FUNCTION\n")
