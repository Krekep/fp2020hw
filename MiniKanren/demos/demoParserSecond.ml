open Minikanren_lib.Parser
open Minikanren_lib.Ast

exception Foo of string

let test_value =
  let temp =
    apply parser
      "run* (q) \n\
      \    (fresh(x y z)\n\
      \        ((== q 1) \n\
      \        (conde (\n\
      \            (== x #f)\n\
      \            (else #f)\n\
      \        ))\n\
      \        (let (x y))\n\
      \    )\n\
       )"
  in
  match temp with
  | Some value -> print_string (program_to_str value)
  | None -> raise (Foo "EXCEPTION IN secondParserTest FUNCTION\n")
