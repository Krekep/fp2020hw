type value =
  | Identifier of string (* pointer to other var*)
  | MyBool of bool (* #t, #f *)
  | MyInt of int (* 1, 5, etc. *)
  | Null
[@@deriving show]

type modifier =
  | Concrete of value
  (* run k *)
  | Any (* run* *)
[@@deriving show]

type names = Name of string [@@deriving show]

type var = Var of names * value [@@deriving show]

type statement =
  | StatementBlock of statement list
  | Fresh of var list * statement (* fresh (x, y, z) *)
  | Let of var (* let (x, #t) *)
  | Conde of statement (* conde ( ((== 1 x) #s) ((== 2 x) #s ()) *)
  | Unify of var (* (== x y) *)
  | Else of value
[@@deriving show]

type program = Run of modifier * var * statement (* run 1 (q) #s (== #t q) *)
[@@deriving show]
