open Opal
open Ast
open Printf

let reserved = [ "#t"; "#f"; "conde"; "else"; "let"; "run"; "fresh" ]

let apply p s = parse p (LazyStream.of_string s)

let ident =
  spaces >> letter <~> many alpha_num => implode >>= function
  | s when List.mem s reserved -> mzero
  | s -> return s

let name = ident >>= fun name -> return (Name name)

let identifier = ident >>= fun identifier -> return (Identifier identifier)

module Expr = struct
  let null = token "null" >> return Null

  let digits = spaces >> many1 digit => implode

  let integer = digits => int_of_string

  let parseInt = integer >>= fun n -> return (MyInt n)

  let parseBool =
    token "#t" >> return (MyBool true)
    <|> (token "#s" >> return (MyBool true))
    <|> (token "#u" >> return (MyBool false))
    <|> (token "#f" >> return (MyBool false))

  let%test _ = apply null "   null" = Some Null

  let%test _ = apply null "null" = Some Null

  let%test _ = apply parseInt "100500" = Some (MyInt 100500)

  let%test _ =
    parse parseInt (LazyStream.of_string "    100500") = Some (MyInt 100500)

  let atomic = parseInt <|> parseBool <|> identifier <|> null

  let%test _ = apply atomic "#t" = Some (MyBool true)
end

let var_list_declaration =
  let name_of_var =
    name >>= fun variable_name -> return (Var (variable_name, Null))
  in
  sep_by1 name_of_var spaces >>= fun variable_decl_list ->
  token ")"
  >> (* let _ =
          print_string "vars declaration - ";
          print_string (vars_to_str (VarDeclarated variable_decl_list));
          print_string "\n"
        in *)
  return variable_decl_list

let var_decl =
  choice
    [
      ( name >>= fun variable_name ->
        spaces >> Expr.atomic >>= fun var_value ->
        token ")" >> return (Var (variable_name, var_value)) );
      ( name >>= fun variable_name ->
        token ")" >> return (Var (variable_name, Null)) );
    ]

let%test _ =
  apply var_list_declaration "  a   b  )"
  = Some [ Var (Name "a", Null); Var (Name "b", Null) ]

module Stat = struct
  open Expr

  let rec statement input =
    choice
      [ block_stat; fresh_stat; conde_stat; unify_stat; let_stat; else_stat ]
      input

  and fresh_stat input =
    ( token "(fresh" >> token "(" >> var_list_declaration >>= fun vars_name ->
      statement >>= fun body -> token ")" >> return (Fresh (vars_name, body)) )
      input

  and conde_stat input =
    ( token "(conde" >> statement >>= fun stat ->
      token ")" >> return (Conde stat) )
      input

  and unify_stat input =
    (token "(==" >> var_decl >>= fun variable -> return (Unify variable)) input

  and block_stat input =
    ( token "(" >> sep_by statement spaces >>= fun block_stat ->
      token ")" >> return (StatementBlock block_stat) )
      input

  and let_stat input =
    ( token "(let" >> token "(" >> var_decl >>= fun variable ->
      token ")" >> return (Let variable) )
      input

  and else_stat input =
    ( token "(else" >> Expr.parseBool >>= fun value ->
      token ")" >> return (Else value) )
      input
end

let run_stat =
  token "run"
  >> choice
       [
         ( Expr.parseInt >>= fun ret ->
           token "(" >> var_decl >>= fun result_var ->
           Stat.statement >>= fun body ->
           return (Run (Concrete ret, result_var, body)) );
         ( token "*" >> token "(" >> var_decl >>= fun result_var ->
           Stat.statement >>= fun body -> return (Run (Any, result_var, body))
         );
       ]

let%test _ =
  apply run_stat "run* (q) ()"
  = Some (Run (Any, Var (Name "q", Null), StatementBlock []))

let%test _ =
  apply run_stat "run 1 (q) ()"
  = Some (Run (Concrete (MyInt 1), Var (Name "q", Null), StatementBlock []))

let parser = run_stat

(* Myself types to string *)
let elem_at_k list k = List.nth list k

let rec program_to_str program =
  match program with
  | Run (m, v, s) ->
      sprintf "Run (\n %s,\n %s,\n %s\n)\n" (modifier_to_str m) (var_to_str v)
        (stat_to_str s)

and modifier_to_str m =
  match m with
  | Concrete x -> sprintf "Modifier(%s)" (value_to_str x)
  | Any -> sprintf "Modifier(*)"

and value_to_str v =
  match v with
  | Identifier n -> sprintf "Identifier(%s)" n
  | MyBool x -> sprintf "MyBool(%B)" x
  | MyInt i -> sprintf "MyInt(%d)" i
  | Null -> sprintf "Null"

and var_to_str v =
  match v with
  | Var (x, y) -> sprintf "Var(%s, %s)" (names_to_str x) (value_to_str y)

and var_list_to_str l =
  sprintf "%s" (String.concat ", " (List.map var_to_str l))

and names_to_str n = match n with Name n -> sprintf "Name(%s)" n

and statlst_to_str l =
  sprintf "%s" (String.concat ", " (List.map stat_to_str l))

and stat_to_str s =
  match s with
  | StatementBlock x -> sprintf "StatementBlock(%s)" (statlst_to_str x)
  | Fresh (v, st) ->
      sprintf "Fresh(%s, %s)" (var_list_to_str v) (stat_to_str st)
  | Let variables -> sprintf "Let(%s)" (var_to_str variables)
  | Conde s -> sprintf "Conde(%s)" (stat_to_str s)
  | Unify v -> sprintf "Unify(%s)" (var_to_str v)
  | Else v -> sprintf "Else(%s)" (value_to_str v)
